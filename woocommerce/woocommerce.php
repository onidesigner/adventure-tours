<?php
/**
 * WooCommerce integration core file.
 *
 * @author    Themedelight
 * @package   Themedelight/AdventureTours
 * @version   3.1.2
 */

$wcIncludesFolder = dirname(__FILE__) . '/includes/';

require $wcIncludesFolder . 'WC_Product_Tour.php';
require $wcIncludesFolder . 'WC_Product_Tour_Variable.php';

require $wcIncludesFolder . 'WC_Tour_Integration_Helper.php';

// To init integration helper.
WC_Tour_Integration_Helper::getInstance();

if ( ! function_exists( 'adventure_tours_init_select2' ) ) {
	function adventure_tours_init_select2() {
		wp_enqueue_script( 'select2' );
		wp_enqueue_style( 'select2', str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/' . 'css/select2.css' );
		// .shipping_method, #calc_shipping_state selectors for selects shipping methods
		// if elements not dom, select2 throw exception "Uncaught query function not defined for Select2" and rendering stopped for next elements in jQuery collections
		TdJsClientScript::addScript( 'initSelect2', 'jQuery(".country_to_state, .select2-selector").select2();' );
	}
}

if ( ! function_exists( 'woocommerce_cart_totals' ) ) {
	function woocommerce_cart_totals() {
		adventure_tours_init_select2();
		wc_get_template( 'cart/cart-totals.php' );
	}
}

if ( ! function_exists( 'adventure_tours_register_woocommerce_widgets' ) ) {
	function adventure_tours_register_woocommerce_widgets() {
		require_once PARENT_DIR . '/woocommerce/widgets/adventure_tours_wc_widget_recent_reviews.php';

		register_widget( 'Adventure_Tours_WC_Widget_Recent_Reviews' );
	}
	add_action( 'widgets_init', 'adventure_tours_register_woocommerce_widgets' );
}

if ( ! function_exists( 'wc_display_item_meta' ) ) {
	/**
	 * Overrides native function of display item meta data to implement order item meta fields filter.
	 * Filter 'adventure_tours_woo3_order_items_meta_get_formatted' used by theme helper to implement tour dates formatting.
	 *
	 * @since  3.0.0
	 * @param  WC_Item $item
	 * @param  array   $args
	 * @return string|void
	 */
	function wc_display_item_meta( $item, $args = array() ) {
		$strings = array();
		$html    = '';
		$args    = wp_parse_args( $args, array(
			'before'    => '<ul class="wc-item-meta"><li>',
			'after'		=> '</li></ul>',
			'separator'	=> '</li><li>',
			'echo'		=> true,
			'autop'		=> false,
		) );

		$formatted_items = apply_filters( 'adventure_tours_woo3_order_items_meta_get_formatted', $item->get_formatted_meta_data(), $item );
		foreach ( $formatted_items as $meta_id => $meta ) {
			$value = $args['autop'] ? wp_kses_post( wpautop( make_clickable( $meta->display_value ) ) ) : wp_kses_post( make_clickable( $meta->display_value ) );
			$strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;
		}

		if ( $strings ) {
			$html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
		}

		$html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );

		if ( $args['echo'] ) {
			echo $html;
		} else {
			return $html;
		}
	}
}


if ( ! function_exists( 'adventure_tours_woocommerce_get_product_schema' ) ) {
	//added to be compatible with WooCommerce < 3.0.1
	/**
	 * Get a products Schema.
	 * @return string
	 */
	function adventure_tours_woocommerce_get_product_schema() {
		global $product;

		$schema = "Product";

		// Downloadable product schema handling
		if ( $product->is_downloadable() ) { // && ! $product->is_type( 'tour') 
			$dtype = version_compare( WC_VERSION, '3.0.0', '<') ? $product->download_type : 'standard';
			switch ( $dtype ) {
				case 'application':
					$schema = "SoftwareApplication";
				break;
				case 'music':
					$schema = "MusicAlbum";
				break;
				default:
					$schema = "Product";
				break;
			}
		}

		return 'http://schema.org/' . $schema;
	}
}
