<?php
/**
 * Partial template used for looping through query results.
 *
 * @author    Themedelight
 * @package   Themedelight/AdventureTours
 * @version   3.0.2
 */

if ( have_posts() ) {

	if ( is_archive() ) {
		the_archive_description( '<!-- archive description --><div>', '</div><!-- archive description -->' );
	}
	echo '<div class="row">';

	while ( have_posts() ) {
		the_post();
		$postType = get_post_type();
		switch ( $postType ) {
		case 'post':
			if ( is_single() ) { get_template_part( 'content' ); }
			else { get_template_part( 'loop', 'post' ); }
			break;

		case 'product':
			wc_get_template_part( 'content-product', get_post_format() );
			break;

		default:
			get_template_part( 'content', $postType );
			break;
		}
	}
	echo '</div>';
	if ( ! is_single() ) {
		adventure_tours_render_pagination();
	}
} else {
	// get_template_part( 'content', 'none' );
}
