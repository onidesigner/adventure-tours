<?php
/**
 * Shortcode [latest_posts] view.
 * For more detailed list see list of shortcode attributes.
 *
 * @var string  $title
 * @var boolean $title_underline
 * @var string  $number
 * @var string  $translate
 * @var srting  $read_more_text
 * @var string  $words_limit
 * @var boolean $ignore_sticky_posts
 * @var string  $css_class
 * @var string  $view
 * @var array   $items
 *
 * @author    Themedelight
 * @package   Themedelight/AdventureTours
 * @version   2.4.0
 */

if ( ! $items ) {
	return '';
}

$render_limit = isset( $number ) && $number > 0 ? $number : 0;

$title_underline_class = ( $title_underline ) ? ' title--underline' : '';

?>

<div class="last-posts<?php if ( ! empty( $css_class ) ) { echo ' ' . esc_attr( $css_class ); }; ?>">
<?php
if ( $title ) {
	echo do_shortcode( '[title text="' . $title . '" size="small" position="center" decoration="on" underline="' . $title_underline . '" style="dark"]' );
}

$placeholder_image = adventure_tours_placeholder_img( $image_size );

$i = 0;
?>
<?php foreach ( $items as $post ) : ?>
	<?php
	$post_link = get_permalink( $post->ID );

	if ($i == 0) :
	$image_html = adventure_tours_get_the_post_thumbnail( $post->ID, 'medium', array('class' => 'entry-thumb') );
	?>
	<div class="post-wrap bg-gradient post-style-1">
		<div class="post-thumb">
			<a href="<?php echo esc_url( $post_link ); ?>" rel="bookmark" title="<?php echo esc_html( $post->post_title ); ?>">
				<?php echo $image_html ? $image_html : $placeholder_image;?>
			</a>
		</div>
		<div class="post-details">
			<h3 class="entry-title post-title">
				<a href="<?php echo esc_url( $post_link ); ?>" rel="bookmark" title="<?php echo esc_html( $post->post_title ); ?>">
					<?php echo esc_html( $post->post_title ); ?>
				</a>
			</h3>
			<div class="post-meta-info">
				<span class="author-date">
					<span class="post-datetime">
						<time class="entry-date updated post-date">
							<?php echo get_the_date();?>
						</time>
					</span>
				</span>
			</div>
		</div>
	</div>
	<?php else : 
	$image_html = adventure_tours_get_the_post_thumbnail( $post->ID, 'thumbnail', array('class' => 'entry-thumb') );
	?>
	<div class="post-wrap post-style-2">
		<div class="post-thumb">
			<a href="<?php echo esc_url( $post_link ); ?>" rel="bookmark" title="<?php echo esc_html( $post->post_title ); ?>">
				<?php echo $image_html ? $image_html : $placeholder_image;?>
			</a>
		</div>
		<div class="post-details">
			<h3 class="entry-title post-title">
				<a href="<?php echo esc_url( $post_link ); ?>" rel="bookmark" title="<?php echo esc_html( $post->post_title ); ?>">
					<?php echo esc_html( $post->post_title ); ?>
				</a>
			</h3>
			<div class="post-meta-info">
				<span class="post-datetime">
					<time class="entry-date updated post-date">
						<?php echo get_the_date();?>
					</time>
				</span>
			</div>
		</div>
	</div>
	<?php endif; $i++; ?>	
	<?php if ( $render_limit > 0 && --$render_limit < 1 ) {
		break;
	} ?>
<?php endforeach; ?>
</div>
