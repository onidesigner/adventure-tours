<?php
/**
 * Content template part.
 *
 * @author    Themedelight
 * @package   Themedelight/AdventureTours
 * @version   3.0.6
 */

$post_id = $item->ID;
$item_url = get_the_permalink( $post_id );
$item_title = get_the_title( $post_id );
$image_html = adventure_tours_get_the_post_thumbnail( $post_id, 'thumb_tour_listing_small' );
$placeholder_image = adventure_tours_placeholder_img( 'thumb_tour_listing_small' );
?>

<article class="col-sm-6 col-md-4" itemscope itemtype="http://schema.org/BlogPosting">

	<div class="atgrid__item">
		<div class="atgrid__item__top">
			<?php printf( '<a href="%s" class="atgrid__item__top__image">%s</a>',
				esc_url( $item_url ),
				$image_html ? $image_html : $placeholder_image
				); ?>
		</div>

		<div class="atgrid__item__content">
			<h3 class="atgrid__item__title"><a href="<?php echo esc_url( $item_url ); ?>"><?php echo esc_html( $item_title ); ?></a></h3>
			<?php if ( $description_words_limit > 0 ) { ?>
			<div class="atgrid__item__description"><?php echo adventure_tours_get_short_description( $item, $description_words_limit ); ?>...</div>
			<?php } ?>
		</div>
	</div>

</article>
